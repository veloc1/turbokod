# -*- coding: utf-8 -*-
"""
Created on Wed Dec  5 21:30:50 2012

@author: korri
"""
import pygame


class Timer:
    def __init__(self):
        self.start = pygame.time.get_ticks()
        
    def get(self):
        # print pygame.time.get_ticks() - self.start
        return (pygame.time.get_ticks() - self.start) / 1000.
        
    def reset(self):
        self.start = pygame.time.get_ticks()
