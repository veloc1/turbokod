# -*- coding: utf-8 -*-
"""
Created on Sat Jan 19 23:48:19 2013

@author: korri
"""
import math
import random
import pygame
from gameobject import GameObject
from bullet import Bullet
from timer import Timer


class Ship(GameObject):
    def __init__(self, x, y, bullets):
        super(Ship, self).__init__(x,y, 10, 10)
        self.color = (88, 222, 255)
        self.velocity = 7
        self.health = 100
        self.trail = []
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
        self.timer = Timer()
        self.col_sound = pygame.mixer.Sound('hit.wav')
        self.shoot_sound = pygame.mixer.Sound('shoot.wav')
        self.col_sound.set_volume(0.5)
        self.shoot_sound.set_volume(0.1)
        self.col_timer = Timer()
        self.score = 0
        self.bullets = bullets
        self.shoot_timer = Timer()
    
    def draw(self, window):
        for t in self.trail:
            t.ddraw(window)
            
        self.ddraw(window)
    
    def set_pos(self, pos):
        an1 = math.atan2(self.y - pos[1] + 5, pos[0] + 5 - self.x)
        an1 = math.degrees(an1)
        self.angle = an1
        self.timer.reset()
        
    def collide(self, enemyes):
        for e in enemyes:
            r = pygame.Rect(e.x, e.y, e.width, e. height)
            if self.rect.colliderect(r):
                self.health -= 22.2
                e.health -= 25
                self.score += 2.5
                if self.col_timer.get() > 0.05:
                    self.col_sound.play()
                    self.col_timer.reset()
                    
    def collide_bon(self, bons):
        for e in bons:
            r = pygame.Rect(e.x, e.y, e.width, e. height)
            if self.rect.colliderect(r):
                return e.apply_effect(self)
    
    def update(self):
        if self.health < 1:
            self.destroy()
            return "end"

        self.move(self.velocity * math.cos(math.radians(self.angle)),\
            -self.velocity * math.sin(math.radians(self.angle)))

        if self.timer is not None:
            if self.timer.get() > 0.5:
                if random.random() > 0.5:
                    self.angle += 8
                else:
                    self.angle -= 8

        if self.x > 640:
            self.x = -self.width
        if self.x < -self.width:
            self.x = 640
        if self.y > 480:
            self.y = -self.height
        if self.y < -self.height:
            self.y = 480
            
        self.trail.append(GameObject(self.x + self.width / 2, \
            self.y + self.height / 2, self.width - 1, self.height - 1, \
            self.color))
        if len(self.trail) > 30:
            self.trail.pop(0)
        for t in self.trail:
            if random.random() > 0.5:
                t.decreaze_size()
                t.decreaze_color()
                
        self.recalc_color()

        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
        
    def shoot(self):
        if self.shoot_timer.get() > 0.02:
            self.bullets.append(Bullet(self.x + self.width / 2, \
                self.y + self.height / 2, self.angle, self))
            self.shoot_timer.reset()
            self.shoot_sound.play()
            self.score -= 0.2
        
    def recalc_color(self):
        self.color = (88, 222, 255 * self.health / 100)
        
    def destroy(self):
        self.health = 0
        self.trail = []
        self.x = - 20
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)