# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 18:44:16 2013

@author: korri
"""
import random
from gameobject import GameObject


class Bonus(GameObject):
    def __init__(self, x, y, enemyes, arr):
        super(Bonus, self).__init__(x, y, 10, 10)
        self.effect = self.choose_effect()
        self.enemyes = enemyes
        self.arr = arr
        
    def choose_effect(self):
        k = random.random() 
        if k < 0.1:
            self.color = (255, 120, 120)
            return "killall"
        elif k >= 0.1 and k < 0.5:
            self.color = (88, 255, 255)
            return "scores"
        else:
            self.color = (255, 250, 120)
            return "heal"
    
    def apply_effect(self, ship=None):
        b=False
        if self.effect == "killall":
            while len(self.enemyes) > 0:
                self.enemyes.pop()
                ship.score += 15.5
            b = True
        if self.effect == "heal":
            ship.health += 50
            if ship.health > 100:
                ship.health = 100
                ship.score += 100
        if self.effect == "scores":
            ship.score += 1000
        self.arr.remove(self)
        self = None
        if b == True:
            return "blick"