# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 23:56:09 2012

@author: korri
"""


class Stack:
    def __init__(self):
        self.stack = []
        
    def push(self, x, y, angle):
        self.stack.append((x,y,angle))
        
    def pop(self):
        return self.stack.pop()
        