# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 13:21:48 2013

@author: korri
"""
import random
import pygame
from gameobject import GameObject
from ship import Ship
from bonus import Bonus


class Enemy(Ship):
    def __init__(self, x=10, y=10, arr=None, wrecks=None, bullets=None, bonuses=None):
        super(Enemy, self).__init__(x, y, bullets)
        self.color = (255, 88, 222)
        self.arr = arr
        self.wrecks = wrecks
        self.timer = None
        self.bonuses = bonuses
        self.vary_size()
        
    def change_angle(self, dir=0):
        if dir > 0:
            self.angle += 10
        elif dir < 0:
            self.angle -= 10
            
    def vary_size(self):
        k = random.random()
        if k > 0.8:
            self.width = self.width * k * 3
            self.height = self.height * k * 3
            self.velocity -= 5
        elif k > 0.5 and k < 0.8:
            self.width = self.width * k * 2
            self.height = self.height * k * 2
            self.velocity -= 2
        else:
            pass
        
    def recalc_color(self):
        self.color = (255 * self.health / 100, 88, 222)
                    
    def destroy(self):
        self.arr.remove(self)
        for i in range(15):
            g = GameObject(self.x + self.width / 2, \
                self.y + self.height / 2, self.width, self.height, \
                self.color)
            g.velocity = random.randint(2,10)
            g.angle = random.randint(-180, 180)
            self.wrecks.append(g)
        if random.random() > 0.3:
            self.bonuses.append(Bonus(self.x, self.y, self.arr, self.bonuses))
        self = None