# -*- coding: utf-8 -*-
"""
Created on Sat Dec 22 23:00:47 2012

@author: korri
"""
import pygame


class Button:
    def __init__(self, x, y, text, event, window):
        self.x = x
        self.y = y
        self.text = text
        self.event = event
        self.window = window
        self.font = pygame.font.SysFont('arial', 16)
        self.create_text(text)
        
    def create_text(self, text):
        self.surf = self.font.render(text, True, (255, 255, 255))
        self.rect = self.surf.get_rect()
        self.rect.left = self.x
        self.rect.top = self.y
        
    def is_clicked(self, pos):
        if pos[0] > self.rect.left and pos[0] < self.rect.right and \
                pos[1] > self.rect.top and pos[1] < self.rect.bottom:
            self.event()
            return True
        
    def draw(self):
        pygame.draw.rect(self.window, (60, 60, 0), self.rect)
        self.window.blit(self.surf, (self.x, self.y))