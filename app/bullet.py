# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 16:17:17 2013

@author: korri
"""
import pygame
from gameobject import GameObject


class Bullet(GameObject):
    def __init__(self, x, y, angle, parent):
        super(Bullet, self).__init__(x, y, 5, 5, (222, 0, 88))
        self.angle = angle
        self.velocity = 10
        self.col_sound = pygame.mixer.Sound('hit.wav')
        self.col_sound.set_volume(0.5)
        self.parent = parent
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
        
    def collide_en(self, enemyes):
        for e in enemyes:
            r = pygame.Rect(e.x, e.y, e.width, e. height)
            if self.rect.colliderect(r):
                e.health -= 5
                self.parent.score += 3.1
                self.col_sound.play()
                
    def update(self):
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)
        self.move_rel()