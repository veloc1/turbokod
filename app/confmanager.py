# -*- coding: utf-8 -*-
"""
Created on Sun Jan  6 16:47:53 2013

@author: korri
"""


class ConfManager:
    def __init__(self, filename):
        self.filename = "data/confs/" + filename + ".conf"
        mfile = open(self.filename, 'r')
        self.lines = mfile.readlines()
        mfile.close()
        
    def get_filename(self):
        return self.filename
        
    def get_param(self, paramname):
        for l in self.lines:
            if paramname in l:
                k = l.split(" = ")
                param = k[1].rstrip("\n")
                return param