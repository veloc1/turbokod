# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 16:22:38 2012

@author: korri
"""
import math
import random
import pygame


class GameObject(object):
    def __init__(self, x=0, y=0, width=30, height=30, color=(255, 0, 255)):
        self.x = x - width / 2
        self.y = y - height / 2
        self.width = width
        self.height = height
        
        self.velocity = 0
        self.angle = 0
        
        self.color = color
        
    def move(self, dx=0, dy=0):
        self.x += dx
        self.y += dy
        self.x = int(self.x)
        self.y = int(self.y)
        
    def move_rel(self):
        self.x += math.cos(math.radians(self.angle))*self.velocity
        self.y -= math.sin(math.radians(self.angle))*self.velocity
            
    def ddraw(self, window):
        pygame.draw.rect(window, self.color, (self.x, self.y, self.width, \
            self.height))
            
    def set_size(self, width, height):
        self.width = width
        self.height = height
        
    def decreaze_vel(self):
        if self.velocity >1:
            self.velocity -= 1
        
    def decreaze_size(self):
        if self.width > 1:
            self.width -= 1
            if random.random() > 0.5:
                self.x -= 1
            else:
                self.x += 1
        if self.height > 1:
            self.height -= 1
            if random.random() > 0.5:
                self.y += 1
            else:
                self.y -= 1
            
    def decreaze_color(self):
        c1 = self.color[0]
        c2 = self.color[1]
        c3 = self.color[2]
        if self.color[0] > 50:
            c1 = self.color[0] - 40
        if self.color[1] > 50:
            c2 = self.color[1] - 40
        if self.color[2] > 50:
            c3 = self.color[2] - 40
        self.color = (c1, c2, c3)