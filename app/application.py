# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 13:48:05 2012

@author: korri
"""
import pygame
from gamescene import GameScene


class Application:
    def __init__(self, width=640, height=480):
        pygame.init()
        self.disply = pygame.display.set_mode((width, height))
        pygame.display.set_caption("TurboKod!")
        
        pygame.mixer.quit()
        pygame.mixer.pre_init(44100, -16, 2, 1024 * 3)
        pygame.mixer.init(44100, -16, 2, 4096)
        
        self.scene = GameScene(self.disply)
        
    def run(self):
        self.scene.update()