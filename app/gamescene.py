# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 13:49:30 2012

@author: korri
"""
import math
import random
import pygame
from pygame.locals import *
from ship import Ship
from enemy import Enemy
from line import Line
from timer import Timer
from highscores import HighScores


class GameScene(object):
    def __init__(self, window):
        self.window = window
        self.fpsClock = pygame.time.Clock()
        self.sysfont = pygame.font.SysFont('arial', 10)
        self.font = pygame.font.Font("Hardpixel.otf", 24)
        self.run = True
        self.add_objects()
        pygame.mixer.music.load("kod1.ogg")
        pygame.mixer.music.play(-1)
        self.destroy_sound = pygame.mixer.Sound("expl.wav")
        self.destroy_sound.set_volume(0.5)
        self.spawn_timer = Timer()
        self.spawn_time = 3
        self.hs = HighScores()
        
        self.scoressurf = self.font.render("Scores:", \
                        True, (255, 88, 88))
        self.scores1surf = self.font.render("Player 10", \
                        True, (255, 88, 88))
        self.scores2surf = self.font.render("Player 10", \
                        True, (255, 88, 88))
        self.scores3surf = self.font.render("Player 10", \
                        True, (255, 88, 88))

    def add_objects(self):
        self.wrecks = []
        self.bullets = []
        self.c1 = Ship(210, 210, self.bullets)
        self.bonuses = []
        self.enemyes = []
        self.len_en = 5
        for i in range(self.len_en):
            self.enemyes.append(Enemy(random.randint(0, 640), \
                random.randint(0, 480), self.enemyes, self.wrecks, \
                self.bullets, self.bonuses))
            
        self.lines = []
        for i in range(20):
            self.lines.append(Line('v', random.randint(0, 480)))
        for i in range(20):
            self.lines.append(Line('h', random.randint(0, 640)))
        self.end_game = False
        self.blick = False
                
    def update(self):
        #update logic here
        while self.run:
            for event in pygame.event.get():
                if event.type == QUIT:
                    self.run = False
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        self.run = False
                    if event.key == pygame.K_r:
                        self.__init__(self.window)
                    if event.key == pygame.K_x:
                        self.hs.send(self.c1.score)
                        
                if event.type == pygame.MOUSEMOTION:
                    self.c1.set_pos(pygame.mouse.get_pos())
            
            if not self.end_game:
                if pygame.mouse.get_pressed()[0]:
                    self.c1.shoot()
            
            for i in self.enemyes:
                if random.random() > 0.6:
                    if random.random() > 0.5:
                        i.change_angle(1)
                    else:
                        i.change_angle(-1)
                    #i.set_pos(pygame.mouse.get_pos())
            
            if not self.end_game:
                k = self.c1.update()
                if k == "end":
                    self.end_game = True
                    scores = self.hs.get()
                    self.scoressurf = self.font.render("Scores:", \
                        True, (255, 88, 88))
                    self.scores1surf = self.font.render(\
                        "%s"%scores[0], True, (255, 88, 88))
                    self.scores2surf = self.font.render(\
                        "%s"%scores[1], True, (255, 88, 88))
                    self.scores3surf = self.font.render(\
                        "%s"%scores[2], True, (255, 88, 88))
                
                self.c1.collide(self.enemyes)
                k = self.c1.collide_bon(self.bonuses)
                if k == "blick":
                    self.blick = True
            for i in self.enemyes:
                i.update()
            
            for b in self.bullets:
                b.collide_en(self.enemyes)
                b.update()
                
            if len(self.bullets) > 50:
                self.bullets.pop(0)
            
            if self.len_en > len(self.enemyes):
                self.destroy_sound.play()
                self.len_en = len(self.enemyes)

            for l in self.lines:
                l.move(math.cos(math.radians(self.c1.angle))*0.5, \
                    -math.sin(math.radians(self.c1.angle))*0.5)
                    
            for w in self.wrecks:
                w.move_rel()
                if random.random() > 0.9:
                    w.decreaze_vel()
                    w.decreaze_size()
                    w.decreaze_color()
                    
            if len(self.wrecks) > 50:
                self.wrecks.pop(0)

            self.render()
            self.fpsClock.tick(60)
            
            if not self.end_game:
                if self.spawn_timer.get() > self.spawn_time:
                    self.spawn()
    
    def render(self):
        #render here
        self.window.fill((22, 22, 22))
        
        for l in self.lines:
            l.draw(self.window)
            
        for w in self.wrecks:
            w.ddraw(self.window)
        
        self.c1.draw(self.window)
        for i in self.enemyes:
                i.draw(self.window)
                
        for b in self.bullets:
            b.ddraw(self.window)
            
        for b in self.bonuses:
            b.ddraw(self.window)
            
        if self.end_game:
            surf = self.font.render("Game Over!", \
                True, (255, 0, 0))
            self.window.blit(surf, (640 / 2 - surf.get_width() / 2, 100))
            surf = self.font.render("Press R to restart, X to send score", \
                True, (255, 0, 0))
            self.window.blit(surf, (640 / 2 - surf.get_width() / 2, 360))
            
            self.window.blit(self.scoressurf, (\
                640 / 2 - self.scoressurf.get_width() / 2, 150))
            self.window.blit(self.scores1surf, (\
                640 / 2 - self.scores1surf.get_width() / 2, 180))
            self.window.blit(self.scores2surf, (\
                640 / 2 - self.scores2surf.get_width() / 2, 210))
            self.window.blit(self.scores3surf, (\
                640 / 2 - self.scores3surf.get_width() / 2, 240))
        
        fpssurf = self.sysfont.render("FPS: %0.1f"%self.fpsClock.get_fps(), \
            True, (255, 255, 255))
        self.window.blit(fpssurf, (10, 10))
        ssurf = self.font.render("SCORE: %0.1f"%self.c1.score, \
            True, (88, 255, 88))
        self.window.blit(ssurf, (640 - ssurf.get_width() - 20, 10))
        hsurf = self.font.render("HEALTH: %0.1f"%self.c1.health, \
            True, (255, 88, 88))
        self.window.blit(hsurf, (640 - hsurf.get_width() - 20, 480 - \
            hsurf.get_height() - 15))
            
        if self.blick:
            self.window.fill((220, 220, 220))
            self.blick = False
        
        pygame.display.flip()
        
        
    def spawn(self):
        if self.spawn_time > 0.5:
            self.spawn_time -= 0.1

        self.spawn_timer.reset()        
        
        self.enemyes.append(Enemy(random.randint(0, 640), \
                random.randint(0, 480), self.enemyes, self.wrecks, \
                self.bullets, self.bonuses))