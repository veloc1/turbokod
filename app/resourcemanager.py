# -*- coding: utf-8 *-*
"""
Created on Thu Nov 29 23:01:38 2012

@author: korri

    Basic class for scene managment. update() must contains render()
    
"""
import pygame
import os


class ResourceManager:
    image_dir = "data/image"
    def __init__(self, data_dir="data",
        image_dir="data/image"):
        self.data_dir = data_dir
        self.image_dir = image_dir
    
    @classmethod
    def get_image(cls, imagename):
        """ Return image with alpha from image_dir. """
        fullname = os.path.join(cls.image_dir, imagename)
        try:
            image = pygame.image.load(fullname)
        except pygame.error:
            print ("Cannot load {0}".format(fullname))
        else:
            #image = image.convert_alpha()
            image = image.convert()

        return image

    @classmethod
    def get_transimage(cls, imagename):
        """ Return image with alpha from image_dir. """
        fullname = os.path.join(cls.image_dir, imagename)
        try:
            image = pygame.image.load(fullname)
        except pygame.error:
            print ("Cannot load {0}".format(fullname))
        else:
            image = image.convert_alpha()
            #image = image.convert()

        return image
