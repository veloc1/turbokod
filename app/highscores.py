# -*- coding: utf-8 -*-
"""
Created on Mon Jan 21 22:39:59 2013

@author: korri
"""
from threading import Thread
import urllib
from Tkinter import *


class HighScores:
    def __init__(self):
        pass
        
    def get(self):
        #f = urllib.urlopen("http://localhost/turbokod/index.php")
        f = urllib.urlopen("http://grue.tf9.ru/turbokod/index.php")
        if f.getcode() == 200:
            s = f.read()
            f.close()
            res = []
            s = s.replace('</br>', '')
            res = s.split('\n')
            return res
        else:
            return ("Can't", "Connect", "To site")
        
    def send(self, scores):
        self.scores = scores
        thread = Thread(target=self.send2)
        thread.start()
    
    def send2(self):
        self.root = Tk()
        self.text = Entry(self.root)
        self.text.pack()

        self.send_button = Button(self.root, text=u'Send', command=self.send_score)
        self.send_button.pack()
        
        self.root.mainloop()
        
    def send_score(self):
        name = self.text.get()
        params = urllib.urlencode({'name': name, 'score': self.scores})
        #f = urllib.urlopen("http://localhost/turbokod/add_score.php", params)
        f = urllib.urlopen("http://grue.tf9.ru/turbokod/add_score.php", params)
        if f.getcode() == 200:
            f.close()
            self.root.destroy()
            return
        self.text.delete(0, END)
        self.text.insert(INSERT, "Can't connect!")
        f.close()