# -*- coding: utf-8 -*-
"""
Created on Sat Dec 15 13:41:35 2012

@author: korri
"""
import pygame
from gameobject import GameObject


class Camera(GameObject):
    def __init__(self, x, y, width, height, x_max, y_max):
        super(Camera, self).__init__(x,y,width,height)
        self.rect = pygame.Rect(x, y, width, height)
        self.x_max = x_max
        self.y_max = y_max
        
    def update(self, x_mid, y_mid):
        self.x = x_mid - self.width / 2
        self.y = y_mid - self.height / 2
        if self.x < 0:
            self.x = 0#self.width / 2 
        if self.y < 0:
            self.y = 0#self.height / 2
        if self.x + self.width > self.x_max:
            self.x = self.x_max - self.width #/ 2
        if self.y + self.height > self.y_max:
            self.y = self.y_max - self.height #/ 2
            
        self.rect = pygame.Rect(self.x, self.y, self.width, self.height)