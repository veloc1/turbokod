# -*- coding: utf-8 -*-
"""
Created on Sun Jan 20 14:26:57 2013

@author: korri
"""
import random
import pygame

class Line:
    def __init__(self, v_or_h='v', param=10):
        self.dir = v_or_h
        if v_or_h == 'v':
            self.x0 = 0
            self.x1 = 640
            self.y0 = param
            self.y1 = param
        if v_or_h == 'h':
            self.x0 = param
            self.x1 = param
            self.y0 = 0
            self.y1 = 480
        self.color = (88, 88, 88)
        
    def draw(self, window):
        pygame.draw.line(window, self.color, (self.x0, self.y0), \
            (self.x1, self.y1), 2)
            
    def move(self, param_x, param_y):
        c1 = self.color[0]
        c2 = self.color[1]
        c3 = self.color[2]
        if c1 > 22:
            c1 -= 3
        if c2 > 22:
            c2 -= 3
        if c3 > 22:
            c3 -= 3
        self.color = (c1, c2, c3)
            
        if self.dir == 'v':
            self.y0 += param_y
            self.y1 += param_y
            if self.y0 > 480:
                self.y0 = self.y1 = 0
            if self.y0 < 0:
                self.y0 = self.y1 = 480
        if self.dir == 'h':
            self.x0 += param_x
            self.x1 += param_x
            if self.x0 > 640:
                self.x0 = self.x1 = 0
            if self.x0 < 0:
                self.x0 = self.x1 = 640
        if random.random() > 0.99:
            k = random.randint(0, 2)
            if k == 0:
                self.color = (random.randint(88, 200), c2, c3)
            if k == 1:
                self.color = (c1, random.randint(88, 200), c3)
            if k == 2:
                self.color = (c1, c2, random.randint(88, 200))