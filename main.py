# -*- coding: utf-8 -*-
"""
Created on Wed Dec 12 16:18:17 2012

@author: korri
"""
from app.application import Application


def main():
    app = Application()
    app.run()

if __name__ == "__main__":
    main()